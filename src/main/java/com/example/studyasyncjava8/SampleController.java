package com.example.studyasyncjava8;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {
    @Resource
    SampleService sampleService;

    private int num = 0;

    @GetMapping("/")
    public List<User> findAll() {
        return sampleService.getAllUser();
    }

    @GetMapping("/add")
    public String save() throws InterruptedException {

        sampleService.addUser(++num);

        return "added-" + num;
    }

}
