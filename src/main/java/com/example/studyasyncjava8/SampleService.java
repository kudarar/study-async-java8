package com.example.studyasyncjava8;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.function.Supplier;

import javax.annotation.Resource;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SampleService {
    @Resource
    UserRepository userRepository;

    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    @Async
    public CompletableFuture<List<User>> getAllUserAsync() throws InterruptedException {
        log.info("UserRepositoryCustom >>");
        Thread.sleep(1000); // 1秒待機

        // データベースから検索結果を取得
        List<User> result = userRepository.findAll();
        log.info("<< UserRepositoryCustom");

        return CompletableFuture.completedFuture(result);
    }

    //@Async
    public void addUser(int num) throws InterruptedException {
        log.info("addUser >>");

        Supplier<Integer> initValueSupplier = () -> num;
        Consumer<Integer> userConsumer = n -> {

            try {
                Thread.sleep(5_000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            userRepository.save(
                    User.builder()
                            .id("id-" + n)
                            .name("name-" + n)
                            .build());
            log.info("saved-" + n);
        };

        CompletableFuture.supplyAsync(initValueSupplier)
                .thenAcceptAsync(userConsumer);

        log.info("<< addUser");
    }

}
