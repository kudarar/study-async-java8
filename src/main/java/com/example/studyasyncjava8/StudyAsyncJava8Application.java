package com.example.studyasyncjava8;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudyAsyncJava8Application {

    public static void main(String[] args) {
        SpringApplication.run(StudyAsyncJava8Application.class, args);
    }
}
